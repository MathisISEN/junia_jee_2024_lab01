package lab.validation;


import org.junit.jupiter.api.Test;

import static lab.validation.utils.TestUtils.*;


public class DrugValidationTests {

    @Test
    public void shouldHaveDrugClass() throws ClassNotFoundException {
        classCreationTest("junia.lab01.model.Drug");
    }

    @Test
    public void shouldHaveCorrectFieldsInDrugClass() throws ClassNotFoundException, NoSuchFieldException {
        shouldHaveField("junia.lab01.model.Drug", "name", "java.lang.String");
        shouldHaveField("junia.lab01.model.Drug", "lab", "java.lang.String");
    }

    @Test
    public void shouldHaveCorrectMethodsInDrugClass() throws ClassNotFoundException, NoSuchMethodException {
        shouldHaveGetterMethod("junia.lab01.model.Drug", "getName", "java.lang.String");
        shouldHaveGetterMethod("junia.lab01.model.Drug", "getLab", "java.lang.String");
        shouldHaveSetterMethod("junia.lab01.model.Drug", "setName", "java.lang.String");
        shouldHaveSetterMethod("junia.lab01.model.Drug", "setLab", "java.lang.String");
    }

    @Test
    public void shouldHaveEmptyConstructorInDrugClass() throws ClassNotFoundException, NoSuchMethodException {
        shouldHaveEmptyConstructor("junia.lab01.model.Drug");
    }

    @Test
    public void shouldHaveArgsConstructorInDrugClass() throws ClassNotFoundException, NoSuchMethodException {
        shouldHaveArgsConstructor("junia.lab01.model.Drug", "java.lang.String", "java.lang.String");
    }


}
