package lab.validation;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static lab.validation.utils.TestUtils.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SessionFilterValidationTests {

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain chain;


    @Mock
    private ServletContext context;

    @Mock
    private HttpSession session;

    @Test
    public void shouldHaveSessionFilterClass() throws ClassNotFoundException {
        classCreationTest("junia.lab01.filter.SessionFilter");
    }

    @Test
    public void shouldHaveHttpFilterExtendForSessionFilterClass() throws ClassNotFoundException {
        classInheritanceTester("junia.lab01.filter.SessionFilter", HttpFilter.class);
    }

    @Test
    public void shouldHaveWebFilterAnnotationOnSessionFilterClass() throws ClassNotFoundException {
        Class<?> myClass = Class.forName("junia.lab01.filter.SessionFilter");
        WebFilter[] annotations = myClass.getAnnotationsByType(WebFilter.class);
        assertThat(annotations).hasSize(1);
        assertThat(annotations[0].urlPatterns()).containsOnly("/*");
    }

    @Test
    public void shouldHaveCorrectMethodsInSessionFilterClass() throws ClassNotFoundException, NoSuchMethodException {
        shouldHaveMethod("junia.lab01.filter.SessionFilter", "doFilter", HttpServletRequest.class, HttpServletResponse.class, FilterChain.class);
    }

    @Test
    public void shouldTransmitIfUrlIsLogin() throws IOException, ServletException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        //GIVEN
        when(request.getServletPath()).thenReturn("/login");
        Class<?> myClass = Class.forName("junia.lab01.filter.SessionFilter");
        Object obj = myClass.getDeclaredConstructor().newInstance();
        Method doFilterMethod = myClass.getDeclaredMethod("doFilter", HttpServletRequest.class, HttpServletResponse.class, FilterChain.class);
        doFilterMethod.setAccessible(true);
        doFilterMethod.invoke(obj, request, response, chain);
        verify(response, never()).sendRedirect(anyString());
        verify(chain, times(1)).doFilter(eq(request), eq(response));
    }

    @Test
    public void shouldTransmitIfUrlIsIndexJsp() throws IOException, ServletException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        when(request.getServletPath()).thenReturn("/index.jsp");
        Class<?> myClass = Class.forName("junia.lab01.filter.SessionFilter");
        Object obj = myClass.getDeclaredConstructor().newInstance();
        Method doFilterMethod = myClass.getDeclaredMethod("doFilter", HttpServletRequest.class, HttpServletResponse.class, FilterChain.class);
        doFilterMethod.setAccessible(true);
        doFilterMethod.invoke(obj, request, response, chain);
        verify(response, never()).sendRedirect(anyString());
        verify(chain, times(1)).doFilter(eq(request), eq(response));
    }

    @Test
    public void shouldTransmitIfAlreadyLoggedIn() throws IOException, ServletException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        //GIVEN
        Object loggedPharmacist = Class.forName("junia.lab01.model.Pharmacist").getDeclaredConstructor(String.class, String.class).newInstance("pharm", "pwd");
        when(session.getAttribute(eq("loggedPharmacist"))).thenReturn(loggedPharmacist);
        when(request.getSession()).thenReturn(session);
        when(request.getServletPath()).thenReturn("/someUrl");
        Class<?> myClass = Class.forName("junia.lab01.filter.SessionFilter");
        Object obj = myClass.getDeclaredConstructor().newInstance();
        Method doFilterMethod = myClass.getDeclaredMethod("doFilter", HttpServletRequest.class, HttpServletResponse.class, FilterChain.class);
        doFilterMethod.setAccessible(true);
        doFilterMethod.invoke(obj, request, response, chain);
        verify(response, never()).sendRedirect(anyString());
        verify(chain, times(1)).doFilter(eq(request), eq(response));
    }

    @Test
    public void shouldBlockAndRedirectToLogIn() throws IOException, ServletException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        //GIVEN
        when(session.getAttribute(eq("loggedPharmacist"))).thenReturn(null);
        when(request.getSession()).thenReturn(session);
        when(request.getServletPath()).thenReturn("/someUrl");
        when(request.getServletContext()).thenReturn(context);
        when(context.getContextPath()).thenReturn("contextPath");
        Class<?> myClass = Class.forName("junia.lab01.filter.SessionFilter");
        Object obj = myClass.getDeclaredConstructor().newInstance();
        Method doFilterMethod = myClass.getDeclaredMethod("doFilter", HttpServletRequest.class, HttpServletResponse.class, FilterChain.class);
        doFilterMethod.setAccessible(true);
        doFilterMethod.invoke(obj, request, response, chain);
        verify(response, times(1)).sendRedirect(request.getServletContext().getContextPath());
        verify(chain, never()).doFilter(eq(request), eq(response));
    }


}
