package lab.validation.utils;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public final class TestUtils {

    private TestUtils() {
    }

    public static void classInheritanceTester(String className, Class<?> parentClass) throws ClassNotFoundException {
        Class<?> myClass = Class.forName(className);
        assertThat(parentClass).isAssignableFrom(myClass);
    }


    public static void classCreationTest(String className) throws ClassNotFoundException {
        Class<?> myClass = Class.forName(className);
        assertThat(myClass).isNotNull();
    }

    public static void classWebServletCheck(String className, String urlPattern) throws ClassNotFoundException {
        Class<?> myClass = Class.forName(className);
        WebServlet[] annotations = myClass.getAnnotationsByType(WebServlet.class);
        assertThat(annotations).hasSize(1);
        assertThat(annotations[0].urlPatterns()).containsOnly(urlPattern);
    }

    public static void shouldHaveMethod(String className,String methodName, Class<?>... parametersType) throws NoSuchMethodException, ClassNotFoundException {
        Class<?> myClass = Class.forName(className);
        Method initMethod = myClass.getDeclaredMethod(methodName, parametersType);
        assertThat(initMethod).isNotNull();
    }

    public static void shouldHaveListField(String className, String fieldName, String listTypeName) throws NoSuchFieldException, ClassNotFoundException {
        Class<?> myClass = Class.forName(className);
        Field field = myClass.getDeclaredField(fieldName);
        assertThat(Modifier.isPrivate(field.getModifiers())).isTrue();
        ParameterizedType listType = (ParameterizedType) field.getGenericType();
        assertThat(field.getType()).isEqualTo(List.class);
        Class<?> typeOfList = Class.forName(listTypeName);
        assertThat(listType.getActualTypeArguments()[0]).isEqualTo(typeOfList);
    }

    public static void shouldHaveField(String className, String fieldName, String fieldTypeName) throws NoSuchFieldException, ClassNotFoundException {
        Class<?> myClass = Class.forName(className);
        Class<?> fieldClass = Class.forName(fieldTypeName);
        Field field = myClass.getDeclaredField(fieldName);
        assertThat(Modifier.isPrivate(field.getModifiers())).isTrue();
        assertThat(field.getType()).isEqualTo(fieldClass);
    }

    public static void shouldHaveGetterMethod(String className, String methodName, String returnTypeName) throws ClassNotFoundException, NoSuchMethodException {
        Class<?> myClass = Class.forName(className);
        Class<?> returnTypeClass = Class.forName(returnTypeName);
        Method getterMethod = myClass.getDeclaredMethod(methodName);
        assertThat(getterMethod.getReturnType()).isEqualTo(returnTypeClass);

    }

    public static void shouldHaveSetterMethod(String className, String methodName, String parameterTypeName) throws ClassNotFoundException, NoSuchMethodException {
        Class<?> myClass = Class.forName(className);
        Class<?> parameterTypeClass = Class.forName(parameterTypeName);
        Method setterMethod = myClass.getDeclaredMethod(methodName,parameterTypeClass);
        assertThat(setterMethod).isNotNull();
    }

    public static void shouldHaveEmptyConstructor(String className) throws ClassNotFoundException, NoSuchMethodException {
        Class<?> myClass = Class.forName(className);
        assertThat(myClass.getConstructor()).isNotNull();
    }

    public static void shouldHaveArgsConstructor(String className,String... argsTypes) throws ClassNotFoundException, NoSuchMethodException {
        Class<?> myClass = Class.forName(className);
        Class<?>[] argsTypeClasses = new Class[argsTypes.length];
        for (int i = 0; i < argsTypes.length; i++) {
            argsTypeClasses[i] = Class.forName(argsTypes[i]);
        }
        assertThat(myClass.getConstructor(argsTypeClasses)).isNotNull();
    }

    public static Object getFieldValueByName(Object object, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        Field field = object.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        return field.get(object);
    }

    public static Method getAccessibleMethod(Class<?> servletClass, String methodName,Class<?>... args) throws NoSuchMethodException {
        Method method = servletClass.getDeclaredMethod(methodName,args );
        method.setAccessible(true);
        return method;
    }

    public static Object getInitializedServletInstance(Class<?> servletClass) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Object obj = servletClass.getDeclaredConstructor().newInstance();
        Method initMethod = servletClass.getDeclaredMethod("init");
        initMethod.invoke(obj);
        return obj;
    }
}
