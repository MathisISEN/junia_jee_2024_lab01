package lab.validation;

import org.junit.jupiter.api.Test;

import static lab.validation.utils.TestUtils.*;


public class PharmacistValidationTests {

    @Test
    public void shouldHavePharmacistClass() throws ClassNotFoundException {
        classCreationTest("junia.lab01.model.Pharmacist");
    }

    @Test
    public void shouldHaveCorrectFieldsInPharmacistClass() throws ClassNotFoundException, NoSuchFieldException {
        shouldHaveField("junia.lab01.model.Pharmacist", "login", "java.lang.String");
        shouldHaveField("junia.lab01.model.Pharmacist", "password", "java.lang.String");
    }

    @Test
    public void shouldHaveCorrectMethodsInPharmacistClass() throws ClassNotFoundException, NoSuchMethodException {
        shouldHaveGetterMethod("junia.lab01.model.Pharmacist", "getLogin", "java.lang.String");
        shouldHaveGetterMethod("junia.lab01.model.Pharmacist", "getPassword", "java.lang.String");
        shouldHaveSetterMethod("junia.lab01.model.Pharmacist", "setLogin", "java.lang.String");
        shouldHaveSetterMethod("junia.lab01.model.Pharmacist", "setPassword", "java.lang.String");
        shouldHaveMethod("junia.lab01.model.Pharmacist","equals",Object.class);
        shouldHaveMethod("junia.lab01.model.Pharmacist","hashCode");
    }

    @Test
    public void shouldHaveEmptyConstructorInPharmacistClass() throws ClassNotFoundException, NoSuchMethodException {
        shouldHaveEmptyConstructor("junia.lab01.model.Pharmacist");
    }

    @Test
    public void shouldHaveArgsConstructorInPharmacistClass() throws ClassNotFoundException, NoSuchMethodException {
        shouldHaveArgsConstructor("junia.lab01.model.Pharmacist", "java.lang.String", "java.lang.String");
    }


}
