package lab.validation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import static lab.validation.utils.TestUtils.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DrugsServletValidationTests {

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private RequestDispatcher dispatcher;

    @Mock
    private ServletContext context;

    @BeforeEach
    public void addMockedBehavioursOnTestObjects() {
        lenient().when(request.getRequestDispatcher(anyString())).thenReturn(dispatcher);
        lenient().when(request.getServletContext()).thenReturn(context);
        lenient().when(context.getContextPath()).thenReturn("contextPath");
        lenient().when(request.getParameter(eq("name"))).thenReturn("Doliprane");
        lenient().when(request.getParameter(eq("lab"))).thenReturn("Sanofi");
    }

    @Test
    public void shouldHaveDrugsServletClass() throws ClassNotFoundException {
        classCreationTest("junia.lab01.controller.DrugsServlet");
    }

    @Test
    public void shouldHaveHttpServletExtendForDrugsServletClass() throws ClassNotFoundException {
        classInheritanceTester("junia.lab01.controller.DrugsServlet", HttpServlet.class);
    }

    @Test
    public void shouldHaveWebServletAnnotationOnDrugsServletClass() throws ClassNotFoundException {
        classWebServletCheck("junia.lab01.controller.DrugsServlet", "/drugs");
    }

    @Test
    public void shouldHaveCorrectMethodsInDrugsServletClass() throws ClassNotFoundException, NoSuchMethodException {
        shouldHaveMethod("junia.lab01.controller.DrugsServlet", "init");
        shouldHaveMethod("junia.lab01.controller.DrugsServlet", "doGet", HttpServletRequest.class, HttpServletResponse.class);
        shouldHaveMethod("junia.lab01.controller.DrugsServlet", "doPost", HttpServletRequest.class, HttpServletResponse.class);
    }

    @Test
    public void shouldHaveCorrectFieldsInDrugsServletClass() throws ClassNotFoundException, NoSuchFieldException {
        shouldHaveListField("junia.lab01.controller.DrugsServlet", "drugs", "junia.lab01.model.Drug");
    }

    @Test
    public void shouldHaveDrugsAfterInit() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        Class<?> servletClass = Class.forName("junia.lab01.controller.DrugsServlet");
        Object obj = getInitializedServletInstance(servletClass);
        Object fieldContent = getFieldValueByName(obj, "drugs");
        assertThat(fieldContent).isNotNull();
        List<?> list = (List<?>) fieldContent;
        assertThat(list).extracting("name", "lab").containsExactly(tuple("Drug1", "Lab1"), tuple("Drug2", "Lab2"));

    }

    @Test
    public void shouldSetDrugsInRequestThenDispatch() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException, ServletException, IOException {
        Class<?> servletClass = Class.forName("junia.lab01.controller.DrugsServlet");
        Object obj = getInitializedServletInstance(servletClass);
        Object fieldContent = getFieldValueByName(obj, "drugs");
        Method doGetMethod = getAccessibleMethod(servletClass, "doGet", HttpServletRequest.class, HttpServletResponse.class);
        doGetMethod.invoke(obj, request, response);
        verify(request, times(1)).setAttribute(eq("drugs"), eq(fieldContent));
        verify(request, times(1)).getRequestDispatcher(eq("DrugsList.jsp"));
        verify(dispatcher, times(1)).forward(eq(request), eq(response));
    }


    @Test
    public void shouldSaveDrugThenRedirect() throws NoSuchFieldException, IllegalAccessException, IOException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException, InstantiationException {
        Class<?> servletClass = Class.forName("junia.lab01.controller.DrugsServlet");
        Object obj = getInitializedServletInstance(servletClass);
        Method doPostMethod = getAccessibleMethod(servletClass, "doPost", HttpServletRequest.class, HttpServletResponse.class);
        doPostMethod.invoke(obj, request, response);
        Object fieldContent = getFieldValueByName(obj, "drugs");
        List<?> drugs = (List<?>) fieldContent;
        assertThat(drugs).hasSize(3);
        assertThat(getFieldValueByName(drugs.get(2), "name")).isEqualTo("Doliprane");
        assertThat(getFieldValueByName(drugs.get(2), "lab")).isEqualTo("Sanofi");
        verify(response, times(1)).sendRedirect(eq(request.getServletContext().getContextPath() + "/drugs"));
    }


}
