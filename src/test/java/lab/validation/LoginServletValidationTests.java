package lab.validation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import static lab.validation.utils.TestUtils.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LoginServletValidationTests {

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private RequestDispatcher dispatcher;

    @Mock
    private ServletContext context;

    @Mock
    private HttpSession session;

    @BeforeEach
    public void addMockedBehavioursOnTestObjects() {
        lenient().when(request.getSession()).thenReturn(session);
        lenient().when(request.getServletContext()).thenReturn(context);
        lenient().when(context.getContextPath()).thenReturn("contextPath");
        lenient().when(request.getParameter(eq("login"))).thenReturn("loginTest");
        lenient().when(request.getParameter(eq("password"))).thenReturn("passwordTest");
        lenient().when(request.getRequestDispatcher(anyString())).thenReturn(dispatcher);
    }

    @Test
    public void shouldHaveLoginServletClass() throws ClassNotFoundException {
        classCreationTest("junia.lab01.controller.LoginServlet");
    }

    @Test
    public void shouldHaveHttpServletExtendForLoginServletClass() throws ClassNotFoundException {
        classInheritanceTester("junia.lab01.controller.LoginServlet", HttpServlet.class);
    }

    @Test
    public void shouldHaveWebServletAnnotationOnLoginServletClass() throws ClassNotFoundException {
        classWebServletCheck("junia.lab01.controller.LoginServlet", "/login");
    }

    @Test
    public void shouldHaveCorrectMethodsInLoginServletClass() throws ClassNotFoundException, NoSuchMethodException {
        shouldHaveMethod("junia.lab01.controller.LoginServlet", "init");
        shouldHaveMethod("junia.lab01.controller.LoginServlet", "doGet",HttpServletRequest.class, HttpServletResponse.class);
        shouldHaveMethod("junia.lab01.controller.LoginServlet", "doPost",HttpServletRequest.class, HttpServletResponse.class);
    }

    @Test
    public void shouldHaveCorrectFieldsInLoginServletClass() throws ClassNotFoundException, NoSuchFieldException {
        shouldHaveListField("junia.lab01.controller.LoginServlet","pharmacists","junia.lab01.model.Pharmacist");
    }

    @Test
    public void shouldHavePharmacistsAfterInit() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        Class<?> servletClass = Class.forName("junia.lab01.controller.LoginServlet");
        Object obj = getInitializedServletInstance(servletClass);
        Object fieldContent = getFieldValueByName(obj,"pharmacists");
        assertThat(fieldContent).isNotNull();
        List<?> list = (List<?>) fieldContent;
        assertThat(list).extracting("login", "password").containsExactly(tuple("pharm1", "password1"), tuple("pharm2", "password2"));
    }

    @Test
    public void shouldLogoutIfUrlIsGood() throws ServletException, IOException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException, ClassNotFoundException {
        lenient().when(request.getQueryString()).thenReturn("logout");
        Class<?> servletClass = Class.forName("junia.lab01.controller.LoginServlet");
        Object obj = getInitializedServletInstance(servletClass);
        Method doGetMethod = getAccessibleMethod(servletClass, "doGet",HttpServletRequest.class,HttpServletResponse.class);
        doGetMethod.invoke(obj,request,response);
        verify(session, times(1)).removeAttribute(eq("loggedPharmacist"));
        verify(response, times(1)).sendRedirect(eq(request.getServletContext().getContextPath()));
    }

    @Test
    public void shouldNotLogoutIfUrlIsBad() throws ServletException, IOException, ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        when(request.getQueryString()).thenReturn("someUrl");
        Class<?> servletClass = Class.forName("junia.lab01.controller.LoginServlet");
        Object obj = getInitializedServletInstance(servletClass);
        Method doGetMethod = getAccessibleMethod(servletClass, "doGet",HttpServletRequest.class,HttpServletResponse.class);
        doGetMethod.invoke(obj,request,response);
        verify(session, never()).removeAttribute(eq("loggedPharmacist"));
        verify(response, never()).sendRedirect(eq(request.getServletContext().getContextPath()));
    }

    @Test
    public void shouldNotLogInIfCredentialsAreWrong() throws ServletException, IOException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException, ClassNotFoundException {
        Class<?> servletClass = Class.forName("junia.lab01.controller.LoginServlet");
        Object obj = getInitializedServletInstance(servletClass);
        Method doPostMethod = getAccessibleMethod(servletClass, "doPost",HttpServletRequest.class,HttpServletResponse.class);
        doPostMethod.invoke(obj,request,response);
        verify(request, times(1)).setAttribute(eq("loginError"), eq("Invalid credentials!"));
        verify(request, times(1)).getRequestDispatcher(eq("/index.jsp"));
        verify(dispatcher, times(1)).forward(eq(request), eq(response));
    }

    @Test
    public void shouldLogInIfCredentialsAreGood() throws ServletException, IOException, ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        //GIVEN
        when(request.getParameter(eq("login"))).thenReturn("pharm1");
        when(request.getParameter(eq("password"))).thenReturn("password1");
        Object loggedPharmacist = Class.forName("junia.lab01.model.Pharmacist").getDeclaredConstructor(String.class, String.class).newInstance("pharm1", "password1");
        Class<?> servletClass = Class.forName("junia.lab01.controller.LoginServlet");
        Object obj = getInitializedServletInstance(servletClass);
        Method doPostMethod = getAccessibleMethod(servletClass, "doPost",HttpServletRequest.class,HttpServletResponse.class);
        doPostMethod.invoke(obj,request,response);
        verify(request, times(1)).removeAttribute(eq("loginError"));
        verify(session, times(1)).setAttribute(eq("loggedPharmacist"), eq(loggedPharmacist));
        verify(response, times(1)).sendRedirect(eq(request.getServletContext().getContextPath() + "/drugs"));

    }

    









}
