# Lab 01

## Intro

With this practical work, you will learn a few things:
* how to clone a project from Gitlab
* how to import a Maven project in your favorite IDE
* develop an entire JEE webapp almost from scratch
* run a webapp in Tomcat 10 (download it [here](https://tomcat.apache.org/download-10.cgi))

## A drugstore story

We have to develop a webapp in order to help pharmacists with their stock of drugs.
The needs are really easy to implement, actually, the pharmacist will be able to use a "secured" webapp. Once authenticated, the app displays a list of the drugs, and a form allows to add some new drugs.

## Starter kit

If you clone this repo, you will get:
* the Maven configuration & structure (`pom.xml` and the `src` folder)
* the unit tests in order to validate your implementation
  * they are in the `src/test/java` directory
  * they do not compile, don't worry
  * it is forbidden to modify them
* the HTML templates your implementation will have to deal with
  * they are in the `src/main/webapp` directory
* you may wonder what are those `.gitkeep` files? These are just markers in order to push the correct directory tree `src/main/java` inside the repo.
  
## Your mission, should you choose to accept it

*Each checkbox below correspond to a unit test you can run in your IDE.*

* **Drug** POJO (Plain Old Java Object)
  1. [ ] Create a new class `junia.lab01.model.Drug`
  2. [ ] This class should have 2 string attributes : `name` and `lab`
  3. [ ] Create the getters and the setters for these attributes
  4. [ ] Create a constructor that takes these attributes as parameters
  5. [ ] Create an empty constructor

* **Pharmacist** POJO 
  1. [ ] Create a new class `junia.lab01.model.Pharmacist`
  2. [ ] This class should have 2 string attributes : `login` and `password`
  3. [ ] Create the getters and the setters for these attributes, you should also create the `equals` and `hashCode` methods
  4. [ ] Create a constructor that takes these attributes as parameters
  5. [ ] Create an empty constructor

* **DrugsServlet** Controller
  1. [ ] Create a new class `junia.lab01.controller.DrugsServlet`
  2. [ ] This servlet should extends `HttpServlet`
  3. [ ] This servlet should be annotated with `@WebServlet`, configured to wire this implementation to the `/drugs` URL pattern.
  4. [ ] This servlet should implement 3 methods : `init`, `doGet` and `doPost`
  5. [ ] This servlet should have a `drugs` field which is a list of `Drug` objects
  6. [ ] When the servlet is initialized, the list should be populated with 2 elements
     * One element with name `Drug1` and lab `Lab1`
     * One element with name `Drug2` and lab `Lab2`
  7. [ ] The implementation of the `doGet` method respects the following steps:
     * it sets a request attribute named `"drugs"` with the `drugs` field declared above as the value
     * it calls the `getRequestDispatcher` method of the `request` object with the string parameter `"DrugsList.jsp"` (which corresponds to the path of the HTML template to send inside the HTTP response)
     * it calls the `forward` method on the RequestDispatcher retrieved above. Guess the parameters thanks to your IDE
  8. [ ] The implementation of the `doPost` method respects the following steps:
     * it reads 2 request parameters named `name` and `lab` and stores the values in variables
     * with those variables, it creates a new `Drug` object and put it in the internal list of the servlet
     * it calls the `sendRedirect` method of the `response` object with the string parameter `req.getServletContext().getContextPath() + "/drugs"` (URL where we want to redirect the browser)
* **LoginServlet** Controller
    1. [ ] Create a new class `junia.lab01.controller.LoginServlet`
    2. [ ] This servlet should extends `HttpServlet`
    3. [ ] This servlet should be annotated with `@WebServlet`, configured to wire this implementation to the `/login` URL pattern.
    4. [ ] This servlet should implement 3 methods : `init`, `doGet` and `doPost`
    5. [ ] This servlet should have a `pharmacists` field which is a list of `Pharmacist` objects
    6. [ ] When the servlet is initialized, the list should be populated with 2 elements
        * One element with login `pharm1` and password `password1`
        * One element with login `pharm2` and password `password2`
    7. [ ] During a GET request, when the QueryString in the URL is equal to "logout", the `loggedPharmacist` session attribute must be deleted and the browser must be redirected to `request.getServletContext().getContextPath()` 
    8. [ ] During a GET request, if the QueryString is empty or has another value, you must ignore it
    9. [ ] During a POST request, which means an authentication request, the implementation is the following:
        * it reads 2 request parameters names `login` and `password`
        * with those variables, it creates a new `Pharmacist` object
        * it checks if the internal list of the servlet contains such an object
        * if yes, it put this object in the `loggedPharmacist` session attribute, remove the `loginError` request attribute then redirect to `request.getServletContext().getContextPath() + "/drugs"`
        * if no, it put `"Invalid credentials!"` in the `loginError` request attribute then displays the `/index.jsp` content with the request dispatcher
* **SessionFilter** filter
  1. [ ] Create a new class `junia.lab01.filter.SessionFilter`
  2. [ ] This filter should extend `HttpFilter`
  3. [ ] This filter should be annotated with `@WebFilter`, configured to wire this implementation to the `/*` URL pattern.
  4. [ ] This filter should implement the `doFilter` method
  5. [ ] If the URL ends with `/login`, the filter just calls the `doFilter` method of the `chain` parameter (*use `request.getServletPath()` to get the end of the URL*)
  6. [ ] If the URL ends with `/index.jsp`, the filter just calls the `doFilter` method of the `chain` parameter
  7. [ ] If the `loggedPharmacist` session attribute exists, the filter just calls the `doFilter` method of the `chain` parameter
  8. [ ] In any other case, the filter should block and redirect the browser to the login form thanks to the `request.getServletContext().getContextPath()` URL



## Once built...

... you should be able to deploy it on a Tomcat and play with your brand new webapp!

# _Good luck!_
 
 
